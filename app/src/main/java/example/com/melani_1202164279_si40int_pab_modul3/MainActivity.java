package example.com.melani_1202164279_si40int_pab_modul3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {


    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    String nama, pekerjaan, kelamin;
    Parcelable savedRecyclerLayoutState;
    String LIST_STATE = "list state";
    ArrayList <Pengguna> listPengguna;
    EditText txt_nama, txt_pekerjaan;
    Spinner  txt_kelamin;
    RecyclerViewAdapter myAdapter;
    RecyclerView myrv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listPengguna = new ArrayList<>();
        listPengguna.add(new Pengguna("Diki", "Frontend Dev",
                "Laki-Laki",R.drawable.laki));
        listPengguna.add(new Pengguna("Melani", "Data Analyst",
                "Perempuan",R.drawable.laki));

        myrv = (RecyclerView) findViewById(R.id.recyclerview_id);
        myAdapter = new RecyclerViewAdapter(this, listPengguna);
        myrv.setAdapter(myAdapter);
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);
        myrv.setLayoutManager(new GridLayoutManager(this, gridColumnCount));
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback
                (ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN
                        | ItemTouchHelper.UP, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {

                //Get the from and to position
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();

                //Swap the items and notify the adapter
                Collections.swap(listPengguna, from, to);
                myAdapter.notifyItemMoved(from, to);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                //Remove the item from the dataset
                listPengguna.remove(viewHolder.getAdapterPosition());

                //Notify the adapter
                myAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(myrv);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogForm();
            }
        });
    }


    private void DialogForm() {
        dialog = new AlertDialog.Builder(MainActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.add_akun_dialog, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);

        txt_nama    = (EditText) dialogView.findViewById(R.id.txt_nama);
        txt_pekerjaan    = (EditText) dialogView.findViewById(R.id.txt_pekerjaan);
        txt_kelamin  = (Spinner) dialogView.findViewById(R.id.txt_kelamin);


        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                nama    = txt_nama.getText().toString();
                pekerjaan    = txt_pekerjaan.getText().toString();
                kelamin = txt_kelamin.getSelectedItem().toString();

                if (kelamin.equals("Laki-Laki"))
                    listPengguna.add(new Pengguna(nama, pekerjaan,
                            kelamin,R.drawable.perempuan));
                else
                    listPengguna.add(new Pengguna(nama, pekerjaan,
                            kelamin,R.drawable.laki));

                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
